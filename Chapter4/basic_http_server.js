var http = require('http');
var server = http.createServer(function(req, res) {
    var body = 'Hello World';
    res.statusCode = 200;
    res.setHeader('Content-Length', body.length);
    res.setHeader('Content-Type', 'text/plain');
    res.end(body);
});
server.listen(3000);