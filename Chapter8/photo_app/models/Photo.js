var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/photo_app', {useMongoClient: true});

mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

var schema = mongoose.Schema({
    name: String,
    path: String
});

var Photo = mongoose.model('Photo', schema);
module.exports = Photo;



