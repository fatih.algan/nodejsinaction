var fs = require('fs')
var log = fs.createWriteStream('/var/log/myapp.log', { flags: 'a' })
var app = connect()
    .use(connect.logger({ format: ':method :url', stream: log }))
    .use('/error', error)
    .use(hello);