var connect = require('connect')
var fs = require('fs');
var app = connect()
var sites = fs.readdirSync('source/sites');
sites.forEach(function(site){
    console.log(' ... %s', site);
    app.use(connect.vhost(site, require('./sites/' + site)));
});
app.listen(3000);