var connect = require('connect');
var app = connect()
    .use(connect.bodyParser())
    .use(function(req, res) {
        // .. do stuff to register the user ..
        res.end('Registered new user: ' + req.body.username);
    });